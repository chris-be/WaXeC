# WaXeC:	Watch & Execute
Copyright (c) 2017 Christophe Marc BERTONCINI (see LICENSE text file).

## Aim
Graphical and basic tool to :
- create "execute command" configuration
- start/stop commands

Commands can be triggered by "file watching".

Idea came from "sass compiling" need. There are a lot of tools/plugins for each language/IDE but I didn't find any "generic solution".

## Usage examples
- watch source files and trigger compilation
- create a list of "scripts" needed time to time (no need to keep/search them in bash history or in memory)

![Configuration example][cfg-example]
![Executing example][exec-example]

[cfg-example]: examples/WaXeC%20-%20Configuration.png
[exec-example]: examples/WaXeC%20-%20Custom%20SHELL.png

## Check stamp
Tested on Debian Stretch, with kernel GNU/Linux 64 bits and OpenJDK 8.

# Dependencies
<table>
<tr>
	<th>Project</th>	<th>Website</th>	<th>License</th>
</tr>
<tr>
	<td>FasterXML</td>	<td>http://fasterxml.com/</td>	<td>Apache License 2.0</td>
</tr>
<tr>
	<td>SIL</td>	<td>https://framagit.org/chris-be/SIL</td>	<td>BSD License</td>
</tr>
</table>

# Keywords
- command execute
- file watcher
- graphical tool
