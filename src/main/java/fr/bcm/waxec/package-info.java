/*
  Copyright 2017-2018 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

/**
 * WaXeC
 * 
 * @see fr.bcm.waxec.Defines
 */
package fr.bcm.waxec;
