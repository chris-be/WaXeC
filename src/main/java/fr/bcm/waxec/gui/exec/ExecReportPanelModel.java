/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import javax.swing.SwingUtilities;

import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.model.GTextLogModel;
import fr.bcm.lib.sil.gui.model.GTextModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.exec.ExecThreadEvent;

/**
 * Model @see ExecReportPanel
 *
 */
public class ExecReportPanelModel extends AGParentModel implements IObserver<ExecThreadEvent> {

	/** Start Date */
	protected GTextModel startDate;

	/** Stop Date */
	protected GTextModel stopDate;

	/** Exit status */
	protected GTextModel exitStatus;

	/** Execution stdout */
	protected GTextLogModel stdOut;

	/** Execution stderr */
	protected GTextLogModel stdErr;

	public GTextModel getStartDate()
	{	return this.startDate;		}

	public GTextModel getStopDate()
	{	return this.stopDate;		}

	public GTextModel getExitStatus()
	{	return this.exitStatus;		}

	public GTextLogModel getStdOut()
	{	return this.stdOut;			}

	public GTextLogModel getStdErr()
	{	return this.stdErr;			}


	public ExecReportPanelModel()
	{
		this.startDate = new GTextModel();
		this.stopDate = new GTextModel();
		this.exitStatus = new GTextModel();
		this.stdOut = new GTextLogModel();
		this.stdErr = new GTextLogModel();

		this.addChilds(this.startDate, this.stopDate, this.exitStatus, this.stdOut, this.stdErr);
	}

	public void reset()
	{
		this.startDate.setText(null);
		this.stopDate.setText(null);
		this.exitStatus.setText(null);
		this.stdOut.setText(null);
		this.stdErr.setText(null);
	}

	public void clearStdOut()
	{
		this.stdOut.setText(null);
		this.fireChildrenChanged(this);
	}

	public void clearStdErr()
	{
		this.stdErr.setText(null);
		this.fireChildrenChanged(this);
	}

	public void observed(ExecThreadEvent event, Object source)
	{
		if(!SwingUtilities.isEventDispatchThread())
		{
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run()
				{
					ExecReportPanelModel.this.handle(event.getCopy());
				}
			});
		}
		else
		{
			this.handle(event);
		}
	}

	protected void handle(final ExecThreadEvent event)
	{
		if(event.getStartTS() != null)
		{
			String msg = Defines.formatTimeStamp(event.getStartTS());
			this.startDate.setText(msg);
		}

		String tmp;
		tmp = event.getStdOut();
		if(tmp != null)
		{
			this.stdOut.appendText(tmp);
		}

		tmp = event.getStdErr();
		if(tmp != null)
		{
			this.stdErr.appendText(tmp);
		}

		if(event.getStopTS() != null)
		{
			String msg = Defines.formatTimeStamp(event.getStopTS());
			this.stopDate.setText(msg);

			msg = (event.getExitStatus() != null) ? event.getExitStatus().toString() : "killed";
			this.exitStatus.setText(msg);
		}

		this.fireChildrenChanged(this);
	}

}