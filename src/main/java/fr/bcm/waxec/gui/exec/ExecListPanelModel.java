/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.model.GTextLogModel;

/**
 * Model @see ExecListPanel
 */
public class ExecListPanelModel extends AGParentModel {

	/** Log */
	private GTextLogModel log;

	public GTextLogModel getLog()
	{
		return this.log;
	}

	public ExecListPanelModel()
	{
		this.log = new GTextLogModel();

		this.addChilds(this.log);
	}

	public void reset()
	{
		this.log.setText(null);
	}

}