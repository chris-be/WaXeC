/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.model.GBooleanModel;
import fr.bcm.lib.sil.gui.model.GTextModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.exec.Configuration;
import fr.bcm.waxec.exec.ExecThread;
import fr.bcm.waxec.exec.ExecThreadEvent;
import fr.bcm.waxec.exec.WatcherThread;
import fr.bcm.waxec.exec.WatcherThreadEvent;

/**
 * Model @see ExecPanel
 *
 */
public class ExecPanelModel extends AGParentModel {

	/** Configuration to run */
	protected Configuration conf;

	/** ProcessBuilder deduced from given configuration */
	protected ProcessBuilder pb;

	/** Command resume */
	protected GTextModel cmd;

	/** Flag started */
	protected GBooleanModel started;

	/** Execution list */
	protected ExecListPanelModel list;

	/** Execution report */
	protected ExecReportPanelModel report;

	protected IObserver<ExecThreadEvent> execObserver;

	/**
	 */
	protected WatcherThread watcherThread;
	protected IObserver<WatcherThreadEvent> watcherObserver;

	protected ExecThread execThread;

	public GTextModel getCmd()
	{	return this.cmd;		}

	public GBooleanModel getStarted()
	{	return this.started;	}

	public ExecListPanelModel getList()
	{	return this.list;		}

	public ExecReportPanelModel getReport()
	{	return this.report;		}


	public ExecPanelModel()
	{
		super();

		this.conf = new Configuration();
		this.pb = null;
		this.cmd = new GTextModel();
		this.started = new GBooleanModel();
		this.list = new ExecListPanelModel();
		this.report = new ExecReportPanelModel();

		this.watcherThread = null;
		this.execThread = null;

		// Events
		this.addChilds(this.cmd, this.list, this.report);

		this.execObserver = new IObserver<ExecThreadEvent>() {
			
			@Override
			public void observed(ExecThreadEvent event, Object source)
			{
				ExecPanelModel.this.observed(event, source);
			}
		};

		this.watcherObserver = new IObserver<WatcherThreadEvent>() {

				@Override
				public void observed(WatcherThreadEvent event, Object source)
				{
					ExecPanelModel.this.observed(event, source);
				}
			};

	}

	public void setFrom(final Configuration toCopy, final Object source)
	{
		DebugBox.predicateNotNull(toCopy);
		this.stop();

		this.conf.set(toCopy);
		this.cmd.setText(ExecPanelModel.createCommandResume(toCopy));
		this.list.reset();
		this.report.reset();

		this.pb = ExecPanelModel.createProcessBuilder(conf);

		this.fireChildrenChanged(source);
	}

	protected static String createCommandResume(final Configuration conf)
	{
		ArrayList<String> infos = new ArrayList<>();
		String tmp;

		if(conf.isWatch())
		{
			tmp = "Watch: " + conf.getWatchDir();
			tmp = tmp + " [" + String.join(" ", conf.getWatchFiles()) + "]";
			infos.add(tmp);
		}

		tmp = conf.getWorkDir();
		if(!tmp.isEmpty())
		{
			tmp = "Work dir: " + tmp;
			infos.add(tmp);
		}

		String cmd = conf.getExec();
		for(String pm : conf.createFinalParams())
		{
			cmd = cmd + " " + pm;
		}
		infos.add(cmd);

		return String.join("\n", infos);
	}

	public static ProcessBuilder createProcessBuilder(final Configuration conf)
	{
		ProcessBuilder pb = new ProcessBuilder();
		pb.command().add(conf.getExec());
		pb.command().addAll(conf.createFinalParams());

		String tmp = conf.getWorkDir();
		if(!CVS_String.isNullOrEmpty(tmp))
		{
			pb.directory(new File(tmp));
		}

		return pb;
	}

	protected void observed(ExecThreadEvent event, Object source)
	{
		if(!SwingUtilities.isEventDispatchThread())
		{
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run()
				{
					ExecPanelModel.this.handle(event, source);
				}
			});
		}
		else
		{
			this.handle(event, source);
		}
	}

	protected void handle(final ExecThreadEvent event, final Object source)
	{
		this.report.observed(event, source);
		if((event.getStopTS() != null) && (this.watcherThread == null))
		{
			this.stop();
		}
	}

	protected void observed(WatcherThreadEvent event, Object source)
	{
		if(!SwingUtilities.isEventDispatchThread())
		{
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run()
				{
					ExecPanelModel.this.handle(event);
				}
			});
		}
		else
		{
			this.handle(event);
		}
	}

	protected void handle(final WatcherThreadEvent event)
	{
		String files = String.join(", ", event.getFileNames());
		String msg = String.format("Modified files: %s", files);
		this.log(msg);

		this.startExecThread();
		this.list.fireChildrenChanged(this);
	}

	// Marquer to know if on new line and empty
	protected boolean logNewLine = true;

	/**
	 * Append new line to log
	 * @param msg
	 */
	protected void logNewLine()
	{
		if(!this.logNewLine)
		{
			this.list.getLog().appendText("\n");
			this.logNewLine = true;
		}
	}

	/**
	 * Append to log (insert next line if needed before)
	 * @param msg
	 */
	protected void logStart(String msg)
	{
		if(!this.logNewLine)
		{
			this.logNewLine();
		}
		String tms = Defines.formatTimeStamp(LocalDateTime.now());
		msg = String.format("[%s] %s", tms, msg);
		this.list.getLog().appendText(msg);
		this.logNewLine = false;
	}

	/**
	 * Append to log
	 * @param msg
	 */
	protected void log(String msg)
	{
		String tms = Defines.formatTimeStamp(LocalDateTime.now());
		msg = String.format(" [%s] %s", tms, msg);
		this.list.getLog().appendText(msg);
		this.logNewLine = false;
	}

//
// Threads...
//

	protected void startWatcherThread()
	{
		if(this.watcherThread != null)
		{
			this.logStart("Restart watcher");

			this.stopWatcherThread();
			this.list.fireChildrenChanged(this);
		}

		if(this.watcherThread == null)
		{
			assert this.conf.isWatch();
			this.watcherThread = new WatcherThread(this.conf.getWatchDir(), this.conf.getWatchFiles());
			this.watcherThread.addObserver(this.watcherObserver);
			this.watcherThread.start();
		}
	}

	protected void stopWatcherThread()
	{
		if(this.watcherThread != null)
		{
			if(this.watcherThread.isAlive())
			{
				this.watcherThread.interrupt();
			}
			this.watcherThread.removeObserver(this.watcherObserver);
			this.watcherThread = null;
		}
	}

	protected void startExecThread()
	{
		if(this.execThread != null)
		{
			this.logStart("Restart");

			this.stopExecThread();
			this.list.fireChildrenChanged(this);
		}

		if(this.execThread == null)
		{
			this.execThread = new ExecThread(this.pb);
			this.execThread.addObserver(this.execObserver);
			this.execThread.start();
		}
	}

	protected void stopExecThread()
	{
		if(this.execThread != null)
		{
			if(this.execThread.isAlive())
			{
				this.execThread.interrupt();
				this.execThread.tryJoin();
			}

			this.execThread.removeObserver(this.execObserver);
			this.execThread = null;
		}
	}

	public void start() throws IOException
	{
		if(this.started.isOn())
		{	// Nothing to do
			return;
		}

		this.logStart("Start");

		if(this.conf.isWatch())
		{
			this.startWatcherThread();
		}
		else
		{
			this.startExecThread();
		}

		this.started.setOn(true);

		this.list.fireChildrenChanged(this);
		this.started.fireModelChanged(this);
	}

	public void stop()
	{
		if(!this.started.isOn())
		{	// Nothing to do
			return;
		}

		this.log("Stop");

		if(this.watcherThread != null)
		{
			this.stopWatcherThread();
		}

		if(this.execThread != null)
		{
			this.stopExecThread();
		}

		this.started.setOn(false);

		this.list.fireChildrenChanged(this);
		this.started.fireModelChanged(this);
	}

}