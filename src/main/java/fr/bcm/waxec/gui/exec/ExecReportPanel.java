/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.link.GLinkerTextComponent;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.gui.common.GuiLinkers;

/**
 * Panel reporting execution
 * 
 */
public class ExecReportPanel extends JPanel implements IViewForModel<ExecReportPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = -1170978048768677818L;

	// UI
	protected GridBagLayout		layout;

	protected JLabel			startLbl;
	protected JTextField		startDate;

	protected JLabel			stopLbl;
	protected JTextField		stopDate;
	
	protected JLabel			exitStatusLbl;
	protected JTextField		exitStatus;

	protected JLabel			stdOutLbl;
	protected JButton			stdOutClear;
	protected JTextArea			stdOut;

	protected JLabel			stdErrLbl;
	protected JButton			stdErrClear;
	protected JTextArea			stdErr;
 
	// Model linkers
	protected GuiLinkers			guiLinkers;
	protected GLinkerTextComponent	fwd_startDate;
	protected GLinkerTextComponent	fwd_stopDate;
	protected GLinkerTextComponent	fwd_exitStatus;
	protected GLinkerTextComponent	fwd_stdOut;
	protected GLinkerTextComponent	fwd_stdErr;

	protected ExecReportPanelModel	model;

	public ExecReportPanel() {

		this.layout = new GridBagLayout();
		this.setLayout(this.layout);

		int y = 0;
		JScrollPane sp;
		GridBagConstraints c;

		this.startLbl = new JLabel("ExecReportPanel.started");
		c = CVS_Layout.createGBC(1, y, 1, 1);
		this.add(this.startLbl, c);

		this.stopLbl = new JLabel("ExecReportPanel.stopped");
		c = CVS_Layout.createGBC(2, y, 1, 1);
		this.add(this.stopLbl, c);

		this.exitStatusLbl = new JLabel("ExecReportPanel.exitStatus");
		c = CVS_Layout.createGBC(3, y, 1, 1);
		this.add(this.exitStatusLbl, c);

		++y;
		this.startDate = new JTextField();
		c = CVS_Layout.createGBC(1, y, 1, 1);
		c.weightx = 1;
		this.add(this.startDate, c);

		this.stopDate = new JTextField();
		c = CVS_Layout.createGBC(2, y, 1, 1);
		c.weightx = 1;
		this.add(this.stopDate, c);
		
		this.exitStatus = new JTextField();
		c = CVS_Layout.createGBC(3, y, 1, 1);
		c.weightx = 1;
		this.add(this.exitStatus, c);

		++y;
		this.stdOutLbl = new JLabel("ExecReportPanel.msg");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.NORTH;
		this.add(this.stdOutLbl, c);
		
		this.stdOutClear = new JButton("ExecReportPanel.clear");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.stdOutClear, c);

		this.stdOut = new JTextArea();
		c = CVS_Layout.createGBC(1, y, 3, 1);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 2;
		c.weighty = 2;
		sp = new JScrollPane(this.stdOut);
		this.add(sp, c);

		++y;
		this.stdErrLbl = new JLabel("ExecReportPanel.err");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.NORTH;
		this.add(this.stdErrLbl, c);

		this.stdErrClear = new JButton("ExecReportPanel.clear");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.stdErrClear, c);

		this.stdErr= new JTextArea();
		c = CVS_Layout.createGBC(1, y, 3, 1);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 2;
		c.weighty = 2;
		sp = new JScrollPane(this.stdErr);
		this.add(sp, c);

		this.startDate.setEditable(false);
		this.stopDate.setEditable(false);
		this.exitStatus.setEditable(false);
		this.stdOut.setEditable(false);
		this.stdErr.setEditable(false);

		// Events
		this.stdOutClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ExecReportPanel.this.actionClearStdOut();
			}
		});

		this.stdErrClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ExecReportPanel.this.actionClearStdErr();
			}
		});

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.startLbl);
		this.guiLinkers.linkTitle(this.stopLbl);
		this.guiLinkers.linkTitle(this.exitStatusLbl);
		this.guiLinkers.linkTitle(this.stdOutLbl);
		this.guiLinkers.linkTitle(this.stdOutClear);
		this.guiLinkers.linkTitle(this.stdErrLbl);
		this.guiLinkers.linkTitle(this.stdErrClear);

		this.fwd_startDate	= new GLinkerTextComponent(this.startDate);
		this.fwd_stopDate	= new GLinkerTextComponent(this.stopDate);
		this.fwd_exitStatus	= new GLinkerTextComponent(this.exitStatus);
		this.fwd_stdOut		= new GLinkerTextComponent(this.stdOut);
		this.fwd_stdErr		= new GLinkerTextComponent(this.stdErr);

		this.model = null;
		this.modelChanged();
	}

	@Override
	public void attachModel(ExecReportPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.fwd_startDate.attachModel(this.model.getStartDate());
		this.fwd_stopDate.attachModel(this.model.getStopDate());
		this.fwd_exitStatus.attachModel(this.model.getExitStatus());
		this.fwd_stdOut.attachModel(this.model.getStdOut());
		this.fwd_stdErr.attachModel(this.model.getStdErr());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.fwd_startDate, this.fwd_stopDate, this.fwd_exitStatus, this.fwd_stdOut, this.fwd_stdErr);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
		}
		else
		{
		}
	}

	protected void actionClearStdOut()
	{
		this.model.clearStdOut();
	}

	protected void actionClearStdErr()
	{
		this.model.clearStdErr();
	}

}