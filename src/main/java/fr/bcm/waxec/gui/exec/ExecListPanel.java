/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.link.GLinkerTextComponent;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * Panel listing all executions
 */
public class ExecListPanel extends JPanel implements IViewForModel<ExecListPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = 8609756191408743730L;

	// UI
	protected GridBagLayout			layout;
	protected JTextArea				log;

	// Model linkers
	protected GLinkerTextComponent	fwd_log;

	protected ExecListPanelModel	model;

	public ExecListPanel() {

		this.layout = new GridBagLayout();
		this.setLayout(this.layout);
		this.setBackground(new Color(0, 0, 255, 128));

		int y = 0;
		GridBagConstraints c;

		this.log = new JTextArea();
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		this.add(this.log, c);

		this.log.setEditable(false);

		// Events

		// Init
		this.fwd_log	= new GLinkerTextComponent(this.log);

		this.model = null;
		this.modelChanged();
	}

	@Override
	public void attachModel(ExecListPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.fwd_log.attachModel(this.model.getLog());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.fwd_log);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
		}
		else
		{
		}
	}

}