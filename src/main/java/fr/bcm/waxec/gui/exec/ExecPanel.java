/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.exec;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.link.GLinkerTextComponent;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.gui.common.GuiLinkers;

/**
 * Panel to start/stop a command
 */
public class ExecPanel extends JPanel implements IViewForModel<ExecPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = 5740899267372274877L;

	// UI
	protected GridBagLayout		layout;

	protected JLabel			cmdLbl;
	protected JTextArea			cmd;

	protected JButton			start;
	protected JButton			stop;

	protected ExecListPanel		list;
	protected ExecReportPanel	report;

	// Model linkers
	protected GLinkerTextComponent	fwd_cmd;
	protected GuiLinkers			guiLinkers;

	protected ExecPanelModel	model;

	public ExecPanel()
	{
		this.layout = new GridBagLayout();
		this.setLayout(this.layout);

		int y = 0;
		JScrollPane sp;
		GridBagConstraints c;

		this.cmdLbl = new JLabel("ExecPanel.cmd");
		c = CVS_Layout.createGBC(0, y, 1, 2);
		this.add(this.cmdLbl, c);

		this.cmd = new JTextArea();
		c = CVS_Layout.createGBC(1, y, 1, 2);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		sp = new JScrollPane(this.cmd);
		this.add(sp, c);

		this.start = new JButton("ExecPanel.start");
		c = CVS_Layout.createGBC(2, y, 1, 1);
		c.anchor = GridBagConstraints.NORTHEAST;
		this.add(this.start, c);

		++y;
		this.stop = new JButton("ExecPanel.stop");
		c = CVS_Layout.createGBC(2, y, 1, 1);
		c.anchor = GridBagConstraints.SOUTHEAST;
		this.add(this.stop, c);

		++y;
		this.list = new ExecListPanel();
		c = CVS_Layout.createGBC(0, y, 3, 1);
		c.weightx = 1;
		c.weighty = 2;
		c.fill = GridBagConstraints.BOTH;
		sp = new JScrollPane(this.list);
		this.add(sp, c);
		//this.add(this.list, c);

		++y;
		this.report = new ExecReportPanel();
		c = CVS_Layout.createGBC(0, y, 3, 1);
		c.weightx = 1;
		c.weighty = 6;
		c.fill = GridBagConstraints.BOTH;
		this.add(this.report, c);

		this.cmd.setEditable(false);

		// Events
		this.start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ExecPanel.this.start();
			}
		});

		this.stop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ExecPanel.this.stop();
			}
		});

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.cmdLbl);
		this.guiLinkers.linkTitle(this.start);
		this.guiLinkers.linkTitle(this.stop);

		this.fwd_cmd	= new GLinkerTextComponent(this.cmd);

		this.model = null;
		this.modelChanged();
	}

	@Override
	public void attachModel(ExecPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.fwd_cmd.attachModel(this.model.getCmd());
		this.list.attachModel(this.model.getList());
		this.report.attachModel(this.model.getReport());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.fwd_cmd, this.list, this.report);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			boolean on = this.model.getStarted().isOn();
			this.start.setEnabled(!on);
			this.stop.setEnabled(on);
		}
		else
		{
			this.start.setEnabled(false);
			this.stop.setEnabled(false);
		}
	}

	protected void start()
	{
		try
		{
			this.model.start();
		}
		catch(Exception e)
		{
			//TODO error
		}
	}

	protected void stop()
	{
		this.model.stop();
	}

}