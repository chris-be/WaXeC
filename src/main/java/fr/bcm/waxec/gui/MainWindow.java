/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import fr.bcm.lib.sil.gui.cpt.TabCloseComponent;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;
import fr.bcm.lib.sil.gui.link.CVS_GLinker;
import fr.bcm.lib.sil.gui.model.GNotBooleanModel;
import fr.bcm.lib.sil.gui.model.GTextModel;
import fr.bcm.waxec.exec.Configuration;
import fr.bcm.waxec.gui.common.GuiLinkers;
import fr.bcm.waxec.gui.common.GuiService;
import fr.bcm.waxec.gui.conf.ManageConfPanel;
import fr.bcm.waxec.gui.exec.ExecPanel;
import fr.bcm.waxec.gui.exec.ExecPanelModel;

/**
 * Main graphical interface
 */
public class MainWindow extends JFrame implements ExecuteListener {

	private static final long serialVersionUID = -7852657599596005205L;

	protected MainToolbar		toolbar;
	protected JTabbedPane		tabs;
	protected ManageConfPanel	manageConfPanel;

	protected GuiLinkers		guiLinkers;

	public MainWindow()
	{
		super("MainWindow.title");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setMinimumSize(new Dimension(700, 500));
		// this.setMaximumSize(new Dimension(2048, 1024));

		this.toolbar = new MainToolbar();
		this.toolbar.setFloatable(false);
		this.toolbar.setRollover(true);
		this.add(this.toolbar, BorderLayout.PAGE_START);

		this.tabs = new JTabbedPane();
		this.add(tabs, BorderLayout.CENTER);

		this.manageConfPanel = new ManageConfPanel();
		this.tabs.addTab("Configuration", null, this.manageConfPanel, "To manage configurations files");

		this.pack();

		// Events
		this.manageConfPanel.addExecuteListener(this);

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this);
	}

	@Override
	public void execute(Configuration configuration, File file)
	{
		String fileName = (file != null) ? file.getPath() : "";
		String title = "Exec [" + fileName + "]";

		ExecPanelModel execModel = new ExecPanelModel();
		execModel.setFrom(configuration, this);

		ExecPanel panel = new ExecPanel();
		panel.attachModel(execModel);

		this.tabs.addTab(title, null, panel, "Execute");
		int li = this.tabs.getTabCount()-1;
		this.tabs.setSelectedIndex(li);
		TabCloseComponent tabCloseBtn = new TabCloseComponent(this.tabs);
		this.tabs.setTabComponentAt(li, tabCloseBtn);

		// @TODO
		GTextModel titleModel = new GTextModel(title);
		CVS_GLinker.titleLinker(tabCloseBtn, titleModel);
		IGLabelModel tooltipModel = GuiService.createTranslatedLabelModel("tab.ExecPanel.tooltip");
		CVS_GLinker.tooltipLinker(tabCloseBtn, tooltipModel);

		GNotBooleanModel stoppedModel = new GNotBooleanModel(execModel.getStarted());
		CVS_GLinker.activeLinker(tabCloseBtn, stoppedModel);

		try
		{
			execModel.start();
		}
		catch(IOException e)
		{
			// Fail silently
		}
	}

}