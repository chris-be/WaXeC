/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui;

import java.awt.Dimension;

import javax.swing.JDialog;

import fr.bcm.waxec.gui.common.GuiLinkers;

/**
 * Main graphical interface
 */
public class AboutDialog extends JDialog {

	private static final long serialVersionUID = -5295937540606955883L;

	protected GuiLinkers		guiLinkers;

	public AboutDialog()
	{
		super();
		this.setTitle("dlg.About.title");
		this.setMinimumSize(new Dimension(580, 380));

		AboutPanel	aboutPanel = new AboutPanel();
		this.add(aboutPanel);

		this.pack();

		// Events

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this);
	}

}