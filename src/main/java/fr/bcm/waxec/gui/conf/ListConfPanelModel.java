/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import java.io.File;

import fr.bcm.lib.sil.file.FileExtensions;
import fr.bcm.lib.sil.gui.ESelectFileMode;
import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.model.GListModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.gui.common.FileSelectPanelModel;

/**
 * Model @see ListConfPanel
 */
public class ListConfPanelModel extends AGParentModel {

	/** Selected directory */
	protected FileSelectPanelModel confDir;

	/** List of config files */
	protected GListModel<String> dirList;

	public FileSelectPanelModel getConfDir()
	{	return this.confDir;	}

	public GListModel<String> getDirList()
	{	return this.dirList;	}


	public ListConfPanelModel()
	{
		this.confDir = new FileSelectPanelModel(ESelectFileMode.DIRECTORIES_ONLY);
		this.dirList = new GListModel<>();

		File currentDir = new File("").getAbsoluteFile();
		this.confDir.setFile(currentDir);

		this.confDir.addObserver(new IObserver<GModelChangedEvent>() {

			@Override
			public void observed(GModelChangedEvent event, Object source)
			{
				ListConfPanelModel.this.updateFileList();
			}
		});

		this.addChilds(this.confDir); // , this.dirList);
	}

	public void updateFileList()
	{
		this.dirList.clear();

		File dir = this.confDir.getFile();
		if(dir == null)
		{
			return;
		}

		if(dir.isDirectory())
		{
			FileExtensions fes = Defines.getConfigExtensions();

			// Scan directory for config
			for(File file : dir.listFiles())
			{
				if(file.isFile())
				{
					String fileName = file.getName();
					if(fes.hasExtension(fileName, true))
					{
						this.dirList.addElement(fileName);
					}
				}
			}
		}
	}

}