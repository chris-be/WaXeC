/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.CVS_File;
import fr.bcm.lib.sil.file.FileExtensions;
import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.CVS_Swing;
import fr.bcm.lib.sil.gui.EValidState;
import fr.bcm.lib.sil.gui.cpt.ActionOnItemListener;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.exec.Configuration;
import fr.bcm.waxec.gui.ExecuteListener;
import fr.bcm.waxec.gui.common.GuiLinkers;

/**
 * Panel to show/edit/save a configuration
 *
 */
public class ManageConfPanel extends JPanel implements IViewForModel<ManageConfPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = 4925405843580366291L;

	protected GridBagLayout		layout;
	protected ListConfPanel		listPanel;
	protected ConfPanel			confPanel;

	protected JButton			save;
	protected JButton			saveAs;
	protected JButton			exec;
	protected GuiLinkers		guiLinkers;

	protected ManageConfPanelModel	model;

	/**
	 * Ctor
	 */
	public ManageConfPanel() {

		this.layout = new GridBagLayout();
		this.setLayout(this.layout);

		int y = 0;
		GridBagConstraints c;
		this.listPanel = new ListConfPanel();
		c = CVS_Layout.createGBC(0, y, 1, 1, 1.00, 1.0, new Insets(0, 0, 0, 0));
		c.fill = GridBagConstraints.BOTH;
		this.add(this.listPanel, c);

		this.confPanel = new ConfPanel();
		c = CVS_Layout.createGBC(1, y, 1, 1, 4.00, 1.0, new Insets(0, 4, 0, 0));
		c.fill = GridBagConstraints.BOTH;
		this.add(this.confPanel, c);

		++y;
		y = this.createActions(y);

		// Events
		this.listPanel.addSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				ManageConfPanel.this.selectConfiguration();
			}
		});

		this.listPanel.addActionOnItemListener(new ActionOnItemListener() {

			@Override
			public void actionItem(int index)
			{
				ManageConfPanel.this.actionItem(index);
			}
		});

		this.save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ManageConfPanel.this.actionSave();
			}
		});

		this.saveAs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ManageConfPanel.this.actionSaveAs();
			}
		});

		this.exec.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ManageConfPanel.this.actionExecute();
			}
		});

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.save);
		this.guiLinkers.linkTitle(this.saveAs);
		this.guiLinkers.linkTitle(this.exec);

		this.model = null;
		this.attachModel(new ManageConfPanelModel());
	}

	protected int createActions(int y) {

		this.save = new JButton("ManageConfPanel.save");
		this.saveAs = new JButton("ManageConfPanel.saveAs");
		this.exec = new JButton("ManageConfPanel.execute");

		JPanel pButtons = new JPanel();
		CVS_Swing.setTransparentBK(pButtons);
		pButtons.add(this.saveAs);
		pButtons.add(this.save);
		pButtons.add(this.exec);

		GridBagConstraints c;
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 0.0);
		this.add(pButtons, c);

		return ++y;
	}

	@Override
	public void attachModel(ManageConfPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.listPanel.attachModel(this.model.getList());
		this.confPanel.attachModel(this.model.getConf());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.listPanel, this.confPanel);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			if(event.isForwarder(this.model.getList()))
			{
				// this.configurationChanged();
			}
			else if(event.isForwarder(this.model.getConf()))
			{
				this.configurationChanged();
			}
			else
			{	// attach model ?
				this.configurationChanged();
			}
		}
		else
		{
			this.save.setEnabled(false);
			this.saveAs.setEnabled(false);
			this.exec.setEnabled(false);
		}
	}

	/**
	 * Add listener
	 * @param listener
	 */
	public void addExecuteListener(ExecuteListener listener)
	{
		DebugBox.predicateNotNull(listener);
		this.listenerList.add(ExecuteListener.class, listener);
	}

	/**
	 * Remove listener
	 * @param listener
	 */
	public void removeExecuteListener(ExecuteListener listener)
	{
		DebugBox.predicateNotNull(listener);
		this.listenerList.remove(ExecuteListener.class, listener);
	}

	protected void configurationChanged()
	{
		ConfPanelModel conf = this.model.getConf();
		assert conf != null : "bad use";

		boolean b;
		//
		b = conf.checkModel() == EValidState.VALID;
		this.exec.setEnabled(b);
		// Check with "original"
		b = this.model.hasConfigurationChanged();
		this.saveAs.setEnabled(b);
		b = b && (this.listPanel.getSelectedFile() != null);
		this.save.setEnabled(b);
	}

	protected void selectConfiguration()
	{
		if(this.model.hasConfigurationChanged())
		{	// Ask for "save" if changed
		//TODO

		}

		File file = this.listPanel.getSelectedFile();
		try
		{
			Configuration conf = (file != null) ? Configuration.loadYAML(file) : new Configuration();
			this.model.setConf(conf, null);
			this.model.fireChildrenChanged(null);
		}
		catch(IllegalArgumentException e)
		{
			String msg = String.format("Couldn't load file: %s", e.getMessage());
			this.showErrorPopup(msg);
		}
	}

	protected void actionItem(int index)
	{
		assert this.model != null && this.model.getList() != null;
		assert this.model.getList().getDirList() != null;

		String filename = this.model.getList().getDirList().get(index);
		File file = new File(filename);
		Configuration conf;

		try
		{
			conf = Configuration.loadYAML(file);
		}
		catch(IllegalArgumentException e)
		{
			String msg = String.format("Couldn't load file: %s", e.getMessage());
			this.showErrorPopup(msg);
			return;
		}

		ConfPanelModel test = new ConfPanelModel();
		test.setFrom(conf, null);
		if(test.checkModel() != EValidState.VALID)
		{
			String msg = String.format("Configuration error: %s", filename);
			this.showErrorPopup(msg);
			return;
		}

		for(ExecuteListener l : this.listenerList.getListeners(ExecuteListener.class))
		{
			l.execute(conf, file);
		}
	}

	protected void actionSave()
	{
		try
		{
			File file = this.listPanel.getSelectedFile();
			if(file == null)
			{
				throw new IllegalArgumentException("Internal error: nothing selected");
			}

			this.saveConfiguration(file);
		}
		catch(IllegalArgumentException e)
		{
			String msg = String.format("Couldn't save file: %s", e.getMessage());
			this.showErrorPopup(msg);
		}

	}

	protected void actionSaveAs()
	{
		JFileChooser	fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileExtensions fes = Defines.getConfigExtensions();
		CVS_Gui.addFileFilters(fc, fes);

		ListConfPanelModel listModel = this.model.getList();
		File dir = listModel.getConfDir().getFile();
		if(dir != null)
		{
			fc.setCurrentDirectory(dir);
		}

		int ret = fc.showOpenDialog(this);
		if(ret != JFileChooser.APPROVE_OPTION)
		{
			return;
		}

		File file = fc.getSelectedFile();
		if(!fes.hasExtension(file, true))
		{
			// TODO Popup: forgot extension, add it ?
			file = CVS_File.addExtension(file, Defines.CONFIG_EXTENSION_YAML);
		}

		try
		{
			this.saveConfiguration(file);
			listModel.updateFileList();
		}
		catch(IllegalArgumentException e)
		{
			String msg = String.format("Couldn't save file: %s", e.getMessage());
			this.showErrorPopup(msg);
		}
	}

	/**
	 * Save current configuration and update original one if succeeded
	 * @param file
	 * @throws IllegalArgumentException
	 */
	protected void saveConfiguration(final File file) throws IllegalArgumentException
	{
		Configuration conf =  new Configuration();
		this.model.getConf().exportTo(conf);
		Configuration.saveYAML(conf, file);
		// Update original
		this.model.setOriginalAsConf();
		this.model.fireChildrenChanged(null);
	}

	protected void actionExecute()
	{
		File file = this.listPanel.getSelectedFile();
		Configuration conf = new Configuration();
		this.model.getConf().exportTo(conf);
		if(this.model.hasConfigurationChanged())
		{
			file = null;
		}

		this.fireExecute(conf, file);
	}

	protected void fireExecute(Configuration conf, File file)
	{
		for(ExecuteListener l : this.listenerList.getListeners(ExecuteListener.class))
		{
			l.execute(conf, file);
		}
	}

	protected void showErrorPopup(String message)
	{
		//TODO Popup
	}

}