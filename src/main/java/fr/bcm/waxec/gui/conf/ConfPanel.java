/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.CVS_Swing;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.link.GLinkerCheckBox;
import fr.bcm.lib.sil.gui.link.GLinkerTextComponent;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Colors;
import fr.bcm.waxec.gui.common.FileSelectPanel;
import fr.bcm.waxec.gui.common.GuiLinkers;

/**
 * Panel to show/edit a configuration
 */
public class ConfPanel extends JPanel implements IViewForModel<ConfPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = 2867339178000982011L;

	// UI
	protected GridBagLayout		layout;

	protected JLabel			descLbl;
	protected JTextArea			desc;

	protected JLabel			execLbl;
	protected FileSelectPanel		exec;
	protected JLabel			paramsLbl;
	protected JTextArea			params;

	protected JLabel			workDirLbl;
	protected FileSelectPanel		workDir;

	protected JLabel			watchOnLbl;
	protected JCheckBox			watchOn;
	protected JLabel			watchDirLbl;
	protected FileSelectPanel		watchDir;
	protected JLabel			watchListLbl;
	protected JTextArea			watchList;

	// Model linkers
	protected GLinkerTextComponent		fwd_desc;
	protected GLinkerTextComponent		fwd_params;
	protected GLinkerTextComponent		fwd_watchList;
	protected GLinkerCheckBox			fwd_watchOn;
	// protected GLinkerActivator		act_watchDir;
	// protected GLinkerActivator		act_watchList;
	protected GuiLinkers		guiLinkers;

	protected ConfPanelModel	model;

	public ConfPanel()
	{
		this.layout = new GridBagLayout();
		this.setLayout(this.layout);
		this.setBackground(Colors.CONF_PANEL_BK_COLOR);

		int y = 0;
		y = this.createExec(y);
		y = this.createWork(y);

		// Events
		
		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.descLbl);
		this.guiLinkers.linkTitle(this.execLbl);
		this.guiLinkers.linkTitle(this.paramsLbl);
		this.guiLinkers.linkTitle(this.workDirLbl);

		this.guiLinkers.linkTitle(this.watchOnLbl);
		this.guiLinkers.linkTitle(this.watchOn);
		this.guiLinkers.linkTitle(this.watchDirLbl);
		this.guiLinkers.linkTitle(this.watchListLbl);

		this.fwd_desc		= new GLinkerTextComponent(this.desc);
		this.fwd_params		= new GLinkerTextComponent(this.params);
		this.fwd_watchList	= new GLinkerTextComponent(this.watchList);
		this.fwd_watchOn	= new GLinkerCheckBox(this.watchOn);

		// this.act_watchDir	= new GLinkerActivator(this.watchDir);
		// this.act_watchList	= new GLinkerActivator(this.watchList);

		this.model = null;
		this.modelChanged();
	}

	protected int createExec(int y) {

		JScrollPane sp;
		GridBagConstraints c;

		this.descLbl = new JLabel("ConfPanel.description");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.descLbl, c);

		this.desc = new JTextArea();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 1.0);
		c.fill = GridBagConstraints.BOTH;
		sp = new JScrollPane(this.desc);
		this.add(sp, c);

		++y;
		this.execLbl = new JLabel("ConfPanel.exec");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.execLbl, c);

		this.exec = new FileSelectPanel();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 0.0);
		this.add(this.exec, c);

		++y;
		this.paramsLbl = new JLabel("ConfPanel.params");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.paramsLbl, c);

		this.params = new JTextArea();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 2.0);
		c.fill = GridBagConstraints.BOTH;
		sp = new JScrollPane(this.params);
		this.add(sp, c);

		++y;
		this.workDirLbl = new JLabel("ConfPanel.workDir");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.workDirLbl, c);

		this.workDir = new FileSelectPanel();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 0.0);
		this.add(this.workDir, c);

		return ++y;
	}

	protected int createWork(int y) {

		JScrollPane sp;
		GridBagConstraints c;
		this.watchOnLbl = new JLabel("ConfPanel.watchMode");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.watchOnLbl, c);

		this.watchOn = new JCheckBox("ConfPanel.watchOn");
		CVS_Swing.setTransparentBK(this.watchOn);
		c = CVS_Layout.createGBC(1, y, 1, 1);
		this.add(this.watchOn, c);

		++y;
		this.watchDirLbl = new JLabel("ConfPanel.watchDir");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.watchDirLbl, c);

		this.watchDir = new FileSelectPanel();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 0.0);
		this.add(this.watchDir, c);

		++y;
		this.watchListLbl = new JLabel("ConfPanel.watchedList");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.watchListLbl, c);

		this.watchList = new JTextArea();
		c = CVS_Layout.createGBC(1, y, 1, 1, 1.0, 1.0);
		c.fill = GridBagConstraints.BOTH;
		sp = new JScrollPane(this.watchList);
		this.add(sp, c);

		return ++y;
	}

	@Override
	public void attachModel(ConfPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.fwd_desc.attachModel(this.model.getDescription());
		this.exec.attachModel(this.model.getExec());
		this.fwd_params.attachModel(this.model.getParams());
		this.workDir.attachModel(this.model.getWorkDir());
		this.fwd_watchOn.attachModel(this.model.getWatchOn());
		this.watchDir.attachModel(this.model.getWatchDir());
		this.fwd_watchList.attachModel(this.model.getWatchList());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.model.removeObserver(this);
		CVS_Gui.detachModel(this.fwd_desc, this.exec, this.fwd_params, this.workDir, this.fwd_watchOn, this.watchDir, this.fwd_watchList);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{

		}
		else
		{
		}

		boolean on = this.watchOn.isSelected();
		this.watchDir.setEnabled(on);
		this.watchList.setEnabled(on);
	}

}