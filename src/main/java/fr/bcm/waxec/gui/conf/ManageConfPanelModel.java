/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import fr.bcm.lib.sil.gui.ESelectFileMode;
import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.model.GBooleanModel;
import fr.bcm.waxec.exec.Configuration;
import fr.bcm.waxec.gui.common.FileSelectPanelModel;

/**
 * Model @see ManageConfPanel
 */
public class ManageConfPanelModel extends AGParentModel {

	/** List */
	protected ListConfPanelModel list;

	/** Configuration */
	protected ConfPanelModel conf;

	/** Selected file */
	protected FileSelectPanelModel selectedFile;

	/** Original state of configuration before editing */
	protected ConfPanelModel originalConf;

	/** conf and originalConf are different */
	protected GBooleanModel configurationChanged;

	public ListConfPanelModel getList()
	{	return this.list;			}

	public ConfPanelModel getConf()
	{	return this.conf;			}

	public FileSelectPanelModel getSelectedFile()
	{	return this.selectedFile;	}

	public ConfPanelModel getOriginalConf()
	{	return this.originalConf;	}


	public ManageConfPanelModel()
	{
		this.list = new ListConfPanelModel();
		this.conf = new ConfPanelModel();

		this.selectedFile = new FileSelectPanelModel(ESelectFileMode.FILES_ONLY);
		this.originalConf = new ConfPanelModel();
		this.configurationChanged = new GBooleanModel();
		this.configurationChanged.setOn(false);

		// Subscribe to childs
		this.addChilds(this.list, this.conf, this.configurationChanged);
		// this.setOriginalAsConf();
	}

	/**
	 * 
	 */
	protected void forwardChildEvent(GModelChangedEvent event, Object source)
	{
		// Check configuration
		this.configurationChanged.setOn(this.conf.isDifferent(this.originalConf));

		// Before forward
		super.forwardChildEvent(event, source);
	}

	/**
	 * Set conf and adjust original too
	 * @param conf
	 */
	public void setConf(final Configuration conf, final Object source)
	{
		this.conf.setFrom(conf, source);
		this.setOriginalAsConf();
	}

	/**
	 * Set original conf as conf
	 */
	public void setOriginalAsConf()
	{
		this.originalConf.set(this.conf);
		this.configurationChanged.setOn(false);
	}

	public boolean hasConfigurationChanged()
	{
		return this.configurationChanged.isOn();
	}

}