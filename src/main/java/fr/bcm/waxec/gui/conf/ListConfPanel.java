/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.CVS_Swing;
import fr.bcm.lib.sil.gui.cpt.ActionOnItemListener;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.model.GListModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Colors;
import fr.bcm.waxec.gui.common.FileSelectPanel;
import fr.bcm.waxec.gui.common.GList;
import fr.bcm.waxec.gui.common.GuiLinkers;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 * Panel to list configuration files
 */
public class ListConfPanel extends JPanel implements IViewForModel<ListConfPanelModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = 2867339178000982011L;

	// UI
	protected GridBagLayout		layout;
	protected JLabel			confDirLbl;
	protected FileSelectPanel	confDir;
	protected GList<String, GListModel<String>>		list;

	// Model linkers
	protected GuiLinkers			guiLinkers;

	protected ListConfPanelModel	model;

	public ListConfPanel()
	{
		this.layout = new GridBagLayout();
		this.setLayout(this.layout);
		this.setBackground(Colors.LIST_PANEL_BK_COLOR);

		int y = 0;
		y = this.createList(y);

		// Events

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.confDirLbl);

		this.model = null;
		this.modelChanged();
	}

	protected int createList(int y) {

		GridBagConstraints c;
		this.confDirLbl = new JLabel("ListConfPanel.confDir");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		this.add(this.confDirLbl, c);

		this.confDir = new FileSelectPanel();
		c = CVS_Layout.createGBC(1, y, 2, 1, 1.0, 0.0);
		this.add(this.confDir, c);

		++y;
		this.list = new GList<>();
		this.list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.list.setLayoutOrientation(JList.VERTICAL);
		c = CVS_Layout.createGBC(0, y, 3, 1, 1.0, 1.0);
		c.fill = GridBagConstraints.BOTH;
		this.add(this.list, c);

		return ++y;
	}

	@Override
	public void attachModel(ListConfPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.confDir.attachModel(this.model.getConfDir());
		this.list.attachModel(this.model.getDirList());
		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.list, this.confDir);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{

		}
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		CVS_Swing.setEnabled(enabled, this.confDir, this.list);
	}

	public void addSelectionListener(ListSelectionListener listener)
	{
		this.list.addListSelectionListener(listener);
	}

	public void removeSelectionListener(ListSelectionListener listener)
	{
		this.list.removeListSelectionListener(listener);
	}

	public void addActionOnItemListener(ActionOnItemListener listener)
	{
		this.list.addActionOnItemListener(listener);
	}

	public void removeActionOnItemListener(ActionOnItemListener listener)
	{
		this.list.removeActionOnItemListener(listener);
	}

	public File getSelectedFile()
	{
		File dir = this.model.getConfDir().getFile();
		if(dir == null)
		{
			return null;
		}

		String name = this.list.getSelectedValue();
		return (name != null) ? new File(dir, name) : null;
	}

}