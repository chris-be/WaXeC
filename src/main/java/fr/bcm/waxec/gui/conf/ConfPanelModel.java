/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.conf;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.ESelectFileMode;
import fr.bcm.lib.sil.gui.EValidState;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.gui.crt.AGParentModel;
import fr.bcm.lib.sil.gui.model.GBooleanModel;
import fr.bcm.lib.sil.gui.model.GTextModel;
import fr.bcm.waxec.exec.Configuration;
import fr.bcm.waxec.gui.common.FileSelectPanelModel;

/**
 * Model @see ConfPanel
 */
public class ConfPanelModel extends AGParentModel {

	/** Description */
	protected GTextModel description;

	/** Command */
	protected FileSelectPanelModel exec;

	/** Parameters */
	protected GTextModel params;

	/** Working directory */
	protected FileSelectPanelModel workDir;

	/** Watch "on" */
	protected GBooleanModel watchOn;

	/** Watched directory */
	protected FileSelectPanelModel watchDir;

	/** Watched files */
	protected GTextModel watchList;

	public GTextModel getDescription()
	{	return this.description;	}

	public FileSelectPanelModel getExec()
	{	return this.exec;			}

	public GTextModel getParams()
	{	return this.params;			}

	public FileSelectPanelModel getWorkDir()
	{	return this.workDir;		}

	public GBooleanModel getWatchOn()
	{	return this.watchOn;		}

	public FileSelectPanelModel getWatchDir()
	{	return this.watchDir;		}

	public GTextModel getWatchList()
	{	return this.watchList;		}

	public ConfPanelModel()
	{
		this.description = new GTextModel();
		this.exec = new FileSelectPanelModel(ESelectFileMode.FILES_IN_PATHS_ONLY);
		this.params = new GTextModel();
		this.workDir = new FileSelectPanelModel(ESelectFileMode.DIRECTORIES_ONLY);

		this.watchOn = new GBooleanModel();
		this.watchDir = new FileSelectPanelModel(ESelectFileMode.DIRECTORIES_ONLY);
		this.watchList = new GTextModel();

		// Subscribe to children
		this.addChilds(this.description, this.exec, this.params, this.workDir, this.watchOn, this.watchDir, this.watchList);
	}

	public EValidState checkModel()
	{
		EValidState state;

		state = this.exec.getValidState();
		if(state != EValidState.VALID)
		{
			return state;
		}

		state = this.workDir.getValidState();
		if(state != EValidState.VALID)
		{
			return state;
		}

		if(this.watchOn.isOn())
		{
			state = this.watchDir.getValidState();
			if(state != EValidState.VALID)
			{
				return state;
			}

			if(this.watchList.isTextNullOrEmpty())
			{
				state = EValidState.ERROR;
			}
		}

		return state;
	}

	public boolean isDifferent(final ConfPanelModel original)
	{
		if(original == null)
		{
			return true;
		}

		return
			   AGModel.isDifferent(this.description, original.description)
			|| AGModel.isDifferent(this.exec, original.exec)
			|| AGModel.isDifferent(this.params, original.params)
			|| AGModel.isDifferent(this.workDir, original.workDir)
			|| AGModel.isDifferent(this.watchOn, original.watchOn)
			|| AGModel.isDifferent(this.watchDir, original.watchDir)
			|| AGModel.isDifferent(this.watchList, original.watchList)
			;
	}

	public void set(final ConfPanelModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);

		this.description.set(toCopy.description);
		this.exec.set(toCopy.exec);
		this.params.set(toCopy.params);
		this.workDir.set(toCopy.workDir);

		this.watchOn.set(toCopy.watchOn);
		this.watchDir.set(toCopy.watchDir);
		this.watchList.set(toCopy.watchList);
	}

	public void setFrom(final Configuration toCopy, final Object source)
	{
		DebugBox.predicateNotNull(toCopy);
		String text;

		this.description.setText(toCopy.getDescription());
		this.exec.setText(toCopy.getExec());
		text = String.join("\n", toCopy.getParams());
		this.params.setText(text);
		this.workDir.setText(toCopy.getWorkDir());

		this.watchOn.setOn(toCopy.isWatch());
		this.watchDir.setText(toCopy.getWatchDir());
		text = String.join("\n", toCopy.getWatchFiles());
		this.watchList.setText(text);

		this.fireChildrenChanged(source);
	}

	public void exportTo(Configuration toSet)
	{
		DebugBox.predicateNotNull(toSet);
		Iterable<String> its;

		toSet.setDescription(this.description.getText());
		toSet.setExec(this.exec.getText());
		its = CVS_String.split(this.params.getText(), "\\n+");
		CVS_Collection.replaceAll(toSet.getParams(), its);
		toSet.setWorkDir(this.workDir.getText());

		toSet.setWatch(this.watchOn.isOn());
		toSet.setWatchDir(this.watchDir.getText());
		its = CVS_String.split(this.watchList.getText(), "\\s+");
		CVS_Collection.replaceAll(toSet.getWatchFiles(), its);
	}

}