/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.CVS_Swing;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.gui.common.GuiLinkers;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 * "About" panel
 */
public class AboutPanel extends JPanel {

	private static final long serialVersionUID = -3315701060892815945L;

	GridBagLayout			layout;

	protected JLabel		titleLbl;
	protected JLabel		authorLbl;
	protected JLabel		author;
	protected JLabel		versionLbl;
	protected JLabel		version;
	protected JLabel		licenseTypeLbl;
	protected JLabel		licenseType;
	protected JTextArea		license;

	protected GuiLinkers	guiLinkers;

	public AboutPanel()
	{
		this.layout = new GridBagLayout();
		this.setLayout(this.layout);

		int y = 0;
		GridBagConstraints c;
		this.titleLbl = new JLabel(Defines.TITLE, SwingConstants.CENTER);
		c = CVS_Layout.createGBC(0, y, 2, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.titleLbl, c);

		++y;
		this.versionLbl = new JLabel("About.version");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.versionLbl, c);

		this.version = new JLabel(Defines.VERSION);
		c = CVS_Layout.createGBC(1, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.version, c);

		++y;
		this.licenseTypeLbl = new JLabel("About.licenseType");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.licenseTypeLbl, c);

		this.licenseType = new JLabel(Defines.LICENSE_TYPE);
		c = CVS_Layout.createGBC(1, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.licenseType, c);

		++y;
		this.license = new JTextArea(Defines.getLicense(), 8, 20);
		this.license.setEditable(false);
		CVS_Swing.disableAutoScroll(this.license);
		c = CVS_Layout.createGBC(0, y, 2, 1, 1.0, 1.0, new Insets(5, 0, 0, 0));
		c.fill = GridBagConstraints.BOTH;
		JScrollPane sp;
		sp = new JScrollPane(this.license);
		this.add(sp, c);
		//this.add(this.license, c);

		++y;
		this.authorLbl = new JLabel("About.author");
		c = CVS_Layout.createGBC(0, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.authorLbl, c);

		this.author = new JLabel(Defines.AUTHOR);
		c = CVS_Layout.createGBC(1, y, 1, 1);
		c.anchor = GridBagConstraints.CENTER;
		this.add(this.author, c);

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(this.versionLbl);
		this.guiLinkers.linkTitle(this.licenseTypeLbl);
		this.guiLinkers.linkTitle(this.authorLbl);		
	}

}