/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import fr.bcm.lib.sil.gui.cpt.ActionList;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGListModel;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * List component
 *
 */
public class GList<TItem, TModel extends IGListModel<TItem>> extends ActionList<TItem> implements IViewForModel<TModel>, IObserver<GModelChangedEvent> {

	private static final long serialVersionUID = -4939505177142473795L;

	// Model
	protected TModel	model;

	public GList()
	{
		super();

		// Init
		this.model = null;
		this.modelChanged();
	}

	@Override
	public void attachModel(TModel model)
	{
		this.detachModel();
		this.model = model;
		this.setModel(model);
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.modelChanged();
		this.model.addObserver(this);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.setModel(null);
		this.model.removeObserver(this);
		this.model = null;
		this.modelChanged();
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.observed(new GModelChangedEvent(this.model), null);
	}

	@Override
	public void observed(final GModelChangedEvent event, final Object source)
	{
		assert source != this : "loop !";

		if(this.model != null)
		{
			assert this.model == event.getModel();
		}
		else
		{
		}
	}

}