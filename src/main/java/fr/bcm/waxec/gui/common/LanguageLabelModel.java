/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;
import fr.bcm.lib.sil.patterns.IObserver;

/**
 * LabelModel for translation
 *  - keep key
 *  - "forward" observers to LanguageModel
 */
public class LanguageLabelModel implements IGLabelModel {

	/** Key */
	protected String		key;
	/** */
	protected LanguageModel	model;

	public LanguageLabelModel(String key, LanguageModel model)
	{
		DebugBox.predicateNotNull(key);
		DebugBox.predicateNotNull(model);
		this.key = key;
		this.model = model;
	}

	@Override
	public void fireModelChanged(Object source)
	{
		this.model.fireModelChanged(source);
	}

	@Override
	public void addObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.addObserver(observer);
	}

	@Override
	public void removeObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.removeObserver(observer);
	}

	@Override
	public String getText()
	{
		return this.model.getTranslation(this.key);
	}

	@Override
	public boolean isTextNullOrEmpty()
	{
		String t = this.model.getTranslation(this.key);
		return CVS_String.isNullOrEmpty(t);
	}

}