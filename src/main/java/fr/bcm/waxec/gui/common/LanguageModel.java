/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.AGModel;
import fr.bcm.lib.sil.i18n.Jar_i18n;
import fr.bcm.waxec.ELanguage;

/**
 * LanguageModel for labels
 */
public class LanguageModel extends AGModel {

	/** Language to use */
	protected ELanguage			language;
	/** */
	protected Jar_i18n			ji18n;
	protected ResourceBundle	rb;

	public LanguageModel(ELanguage language) throws IllegalArgumentException, IOException
	{
		this.ji18n = new Jar_i18n();
		this.setLanguage(language);
	}

	public ELanguage getLanguage()
	{
		return this.language;
	}

	public boolean setLanguage(ELanguage lg)
	{
		DebugBox.predicateNotNull(lg);
		
		boolean changed = (this.language != lg); 
		if(changed)
		{
			this.language = lg;
			// English is "default" (no file suffix). If not "forced" System.Locale is used
			Locale loc = (this.language != ELanguage.ENGLISH) ? this.language.getLocale() : Locale.ROOT;
			// Need to clear cache before !
			this.ji18n.clearCache();
			this.rb = this.ji18n.getResourceBundle("Waxec", loc);

			this.wasModified = true;
		}

		return changed;
	}

	public String getTranslation(String key)
	{
		return this.rb.getString(key);
	}

}