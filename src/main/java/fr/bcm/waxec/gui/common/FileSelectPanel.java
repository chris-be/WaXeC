/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;

import fr.bcm.lib.sil.gui.CVS_Gui;
import fr.bcm.lib.sil.gui.CVS_Layout;
import fr.bcm.lib.sil.gui.CVS_Swing;
import fr.bcm.lib.sil.gui.cpt.ValidTextField;
import fr.bcm.lib.sil.gui.crt.IViewForModel;
import fr.bcm.lib.sil.gui.link.GLinkerTextComponent;
import fr.bcm.lib.sil.gui.link.GLinkerValidComponent;

import javax.swing.JFileChooser;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Component with TextField and Button opening a File Chooser
 *
 */
public class FileSelectPanel extends JPanel implements IViewForModel<FileSelectPanelModel> {

	private static final long serialVersionUID = 2018490739789159816L;

	// UI
	protected GridBagLayout		layout;
	protected ValidTextField	textField;
	protected JButton			button;

	// Model linkers
	protected GLinkerTextComponent	fwd_filename;
	protected GLinkerValidComponent	fwd_validFile;

	private FileSelectPanelModel	model;

	public FileSelectPanel()
	{
		// Layout
		this.layout = new GridBagLayout();
		this.setLayout(this.layout);
		CVS_Swing.setTransparentBK(this);

		GridBagConstraints c;
		this.textField = new ValidTextField();
		c = CVS_Layout.createGBC(0, 0, 1, 1, 1.0, 0.0);
		this.add(this.textField, c);

		this.button = new JButton("...");
		c = CVS_Layout.createGBC(1, 0, 1, 1);
		this.add(this.button, c);

		// Events
		this.button.addActionListener(e -> this.chooseFile());

		// Init
		fwd_filename	= new GLinkerTextComponent(this.textField);
		fwd_validFile	= new GLinkerValidComponent(this.textField);

		this.model = null;
		this.modelChanged();
	}

	@Override
	public void attachModel(FileSelectPanelModel model)
	{
		this.detachModel();
		this.model = model;
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		this.fwd_filename.attachModel(this.model);
		this.fwd_validFile.attachModel(this.model);
		// Enable component
		this.setEnabled(true);
	}

	@Override
	public void detachModel()
	{
		if(this.model == null)
		{	// Nothing to do
			return;
		}

		CVS_Gui.detachModel(this.fwd_filename, this.fwd_validFile);
		this.model = null;
		// Disable component
		this.setEnabled(false);
	}

	protected void modelChanged()
	{
		this.setEnabled(this.model != null);
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		CVS_Swing.setEnabled(enabled, this.textField, this.button);
	}

	protected void chooseFile()
	{
		assert this.model != null : "isEnabled should be false";

		JFileChooser	fc = new JFileChooser();
		CVS_Swing.setMode(fc, this.model.getSelectMode());

		File	file = null;
		String t = this.textField.getText();
		if(t != null)
		{
			file = new File(t);
			fc.setCurrentDirectory(file);
		}

		int ret = fc.showOpenDialog(this);
		if(ret == JFileChooser.APPROVE_OPTION)
		{
			file = fc.getSelectedFile();
			this.model.setFile(file);
			this.model.fireModelChanged(this);
		}
	}

}