/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import java.io.File;

import fr.bcm.lib.sil.CVS_String;
import fr.bcm.lib.sil.CVS_System;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.ESelectFileMode;
import fr.bcm.lib.sil.gui.EValidState;
import fr.bcm.lib.sil.gui.crt.IGValidStateModel;
import fr.bcm.lib.sil.gui.model.GTextModel;

/**
 * Model for FileSelectPanel
 *
 */
public class FileSelectPanelModel extends GTextModel implements IGValidStateModel {

	// Mode for selecting file
	protected ESelectFileMode	selectMode;
	// File is valid
	protected EValidState		validState;

	public ESelectFileMode getSelectMode()
	{
		return this.selectMode;
	}

	public EValidState	getValidState()
	{
		return this.validState;
	}

	public FileSelectPanelModel()
	{
		this(ESelectFileMode.FILE_AND_DIRECTORY);
	}

	public FileSelectPanelModel(final ESelectFileMode selectMode)
	{
		super();
		this.selectMode = selectMode;
		this.validState = null;
		this.updateValidState();
	}

	public void set(final FileSelectPanelModel toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		this.setSelectMode(toCopy.getSelectMode());
		super.set(toCopy);
	}

	public boolean setSelectMode(ESelectFileMode selectMode)
	{
		boolean changed = (this.selectMode != selectMode);
		if(changed)
		{
			this.selectMode = selectMode;
			this.wasModified = true;
			this.updateValidState();
		}
		return changed;
	}

	protected void updateValidState()
	{
		// Called when text or mode is modified => no need to update dirty flag
		this.validState = this.checkModel();
	}

	public File getFile()
	{
		return (CVS_String.isNullOrEmpty(this.text)) ? null : new File(this.text);
	}

	public boolean setFile(File file)
	{
		String text = (file != null) ? file.getPath() : null;
		return super.setText(text);
	}

	protected EValidState checkModel()
	{
		EValidState state;

		File file = this.getFile();
		switch(this.selectMode)
		{
			case FILE_AND_DIRECTORY:
			{
				if(file == null)
				{
					return null;
				}
				state = file.exists() ? EValidState.VALID : EValidState.ERROR;
			} break;
			case DIRECTORIES_ONLY:
			{
				if(file == null)
				{
					return EValidState.VALID;
				}
				state = file.isDirectory() ? EValidState.VALID : EValidState.ERROR;
			} break;
			case FILES_ONLY:
			{
				if(file == null)
				{
					return EValidState.ERROR;
				}
				state = file.isFile() ? EValidState.VALID : EValidState.ERROR;
			} break;
			case FILES_IN_PATHS_ONLY:
			{
				if(file == null)
				{
					return EValidState.ERROR;
				}
				if(file.exists())
				{	// Permit absolute path
					state = EValidState.VALID;
				}
				else
				{	// Scan PATH
					File test = FileSelectPanelModel.findFileInPath(file);
					state = (test != null) ? EValidState.VALID : EValidState.ERROR;
				}
			} break;
			default:
			{
				assert false : "Unkown enum";
				return EValidState.ERROR;
			}
		}

		return state;
	}

	@Override
	public boolean setText(String text)
	{
		boolean changed = super.setText(text);
		if(changed)
		{
			this.updateValidState();
		}
		return changed;
	}

	/**
	 * Scan user PATH for a file
	 * 
	 * @param file File to find
	 * @return File with path where it lies
	 * 
	 * TODO: put in a CVS_ ?
	 */
	public static File findFileInPath(final File file)
	{
		DebugBox.predicateNotNull(file);

		String filename = file.getPath();
		for(String path : CVS_System.getUserPaths())
		{
			File test = new File(path, filename);
			if(test.exists() && test.isFile())
			{
				return test;
			}
		}

		return null;
	}

}