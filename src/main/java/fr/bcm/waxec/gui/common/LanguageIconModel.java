/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import javax.swing.ImageIcon;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.GModelChangedEvent;
import fr.bcm.lib.sil.gui.crt.IGIconModel;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.waxec.Defines;
import fr.bcm.waxec.ELanguage;

/**
 * IconModel for "flags"
 *  - load icon
 *  - "forward" observers to LanguageModel
 */
public class LanguageIconModel implements IGIconModel {

	/** */
	protected LanguageModel	model;

	public LanguageIconModel(LanguageModel model)
	{
		DebugBox.predicateNotNull(model);
		this.model = model;
	}

	@Override
	public void fireModelChanged(Object source)
	{
		this.model.fireModelChanged(source);
	}

	@Override
	public void addObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.addObserver(observer);
	}

	@Override
	public void removeObserver(IObserver<GModelChangedEvent> observer)
	{
		this.model.removeObserver(observer);
	}

	@Override
	public ImageIcon getIcon()
	{
		ELanguage lg = this.model.getLanguage();
		return Defines.getFlagIcon(lg);
	}

	@Override
	public boolean setIcon(ImageIcon icon)
	{
		return false;
	}

}