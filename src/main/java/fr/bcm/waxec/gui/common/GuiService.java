/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.gui.crt.IGLabelModel;
import fr.bcm.waxec.ELanguage;

/**
 * GuiService: service for gui
 */
public class GuiService {

	/** App language */
	private static LanguageModel	current;

	/**
	 * Service
	 * @return null if problem
	 */
	public static LanguageModel getCurrentLanguage()
	{
		if(GuiService.current == null)
		{
			ELanguage lg = ELanguage.getDefault();
			try
			{	// Avoid throws injection in every panel
				GuiService.current = new LanguageModel(lg); 
			}
			catch(Throwable t)
			{
			}
		}

		return GuiService.current;
	}

	public static IGLabelModel createTranslatedLabelModel(String key)
	{
		LanguageModel lgm = GuiService.getCurrentLanguage();
		DebugBox.predicateNotNull(lgm, "Shouldn't be");

		return new LanguageLabelModel(key, lgm);
	}

}