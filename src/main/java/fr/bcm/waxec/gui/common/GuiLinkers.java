/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui.common;

import javax.swing.AbstractButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fr.bcm.lib.sil.gui.crt.IGLabelModel;
import fr.bcm.lib.sil.gui.crt.IGLinker;
import fr.bcm.lib.sil.gui.link.CVS_GLinker;
import fr.bcm.lib.sil.gui.link.GLinkerList;

/**
 * GLinkerList for WaXec needs (link title to language model)
 *
 *  @Convenience
 */
public class GuiLinkers extends GLinkerList {

	public GuiLinkers()
	{
		super();
	}

	/** Assume title is key ! */
	public void linkTitle(JFrame comp)
	{
		String key = comp.getTitle();
		IGLabelModel model = GuiService.createTranslatedLabelModel(key);
		IGLinker<?> linker = CVS_GLinker.titleLinker(comp, model);
		this.add(linker);
	}

	/** Assume text is key ! */
	public void linkTitle(JDialog comp)
	{
		String key = comp.getTitle();
		IGLabelModel model = GuiService.createTranslatedLabelModel(key);
		IGLinker<?> linker = CVS_GLinker.titleLinker(comp, model);
		this.add(linker);
	}

	/** Assume text is key ! */
	public void linkTitle(JLabel comp)
	{
		String key = comp.getText();
		IGLabelModel model = GuiService.createTranslatedLabelModel(key);
		IGLinker<?> linker = CVS_GLinker.titleLinker(comp, model);
		this.add(linker);
	}

	/** Assume text is key ! */
	public void linkTitle(AbstractButton comp)
	{
		String key = comp.getText();
		IGLabelModel model = GuiService.createTranslatedLabelModel(key);
		IGLinker<?> linker = CVS_GLinker.titleLinker(comp, model);
		this.add(linker);
	}

}