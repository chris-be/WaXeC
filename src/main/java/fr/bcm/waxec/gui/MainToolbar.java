/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JToolBar;

import fr.bcm.lib.sil.gui.link.CVS_GLinker;
import fr.bcm.waxec.ELanguage;
import fr.bcm.waxec.gui.common.GuiLinkers;
import fr.bcm.waxec.gui.common.GuiService;
import fr.bcm.waxec.gui.common.LanguageIconModel;
import fr.bcm.waxec.gui.common.LanguageModel;

/**
 * Toolbar for main window
 * 
 * TODO create and use ChooseLanguageDialog
 */
public class MainToolbar extends JToolBar {

	private static final long serialVersionUID = -7690880109479512736L;

	protected GuiLinkers		guiLinkers;

	/** Non modal dialog: keep reference to prevent showing infinite "about dialogs" */
	protected AboutDialog		aboutDialog;

	public MainToolbar()
	{
		super();
		this.aboutDialog = null;

		JButton chooseLanguageBtn = new JButton();
		this.add(chooseLanguageBtn);

		JButton aboutBtn = new JButton("toolbar.Main.about");
		aboutBtn.getAccessibleContext().setAccessibleDescription("Open about dialog");
		this.add(aboutBtn);

		// Events
		aboutBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				MainToolbar.this.openAboutDialog();
			}
		});

		chooseLanguageBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				LanguageModel l = GuiService.getCurrentLanguage();
				ELanguage toSel = l.getLanguage().getNext();
				if(l.setLanguage(toSel))
				{
					l.fireModelChanged(this);
				}
			}
		});

		// Init
		this.guiLinkers = new GuiLinkers();
		this.guiLinkers.linkTitle(aboutBtn);
		LanguageIconModel lgIcModel = new LanguageIconModel(GuiService.getCurrentLanguage());
		this.guiLinkers.add(CVS_GLinker.iconLinker(chooseLanguageBtn, lgIcModel));
	}

	protected void openAboutDialog()
	{
		if(this.aboutDialog != null)
		{
			if(this.aboutDialog.isFocusable() && !this.aboutDialog.hasFocus())
			{
				this.aboutDialog.requestFocus();
			}

			return;
		}

		this.aboutDialog = new AboutDialog();

		this.aboutDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.aboutDialog.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e)
			{
				MainToolbar.this.aboutDialog = null;
				super.windowClosing(e);
			}

		});

		this.aboutDialog.setVisible(true);
	}
}