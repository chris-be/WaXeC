/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.gui;

import java.io.File;
import java.util.EventListener;

import fr.bcm.waxec.exec.Configuration;

/**
 * 
 */
public interface ExecuteListener extends EventListener {

	/**
	 *
	 * @param configuration
	 * @param file File from where configuration was loaded (can be null)
	 */
	void execute(final Configuration configuration, final File file);

}