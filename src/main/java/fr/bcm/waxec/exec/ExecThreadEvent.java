/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.exec;

import java.time.LocalDateTime;

import fr.bcm.lib.sil.patterns.IEvent;

/**
 * @see ExecThread
 */
public class ExecThreadEvent implements IEvent {

	/** Start timestamp */
	protected LocalDateTime startTS;

	/** Stop timestamp */
	protected LocalDateTime stopTS;

	/** Permits null */
	protected Integer exitStatus;

	protected String stdOut;

	protected String stdErr;

	public LocalDateTime getStartTS()
	{	return this.startTS;	}

	public void setStartTS(LocalDateTime v)
	{	this.startTS = v;		}

	public LocalDateTime getStopTS()
	{	return this.stopTS;		}

	public void setStopTS(LocalDateTime v)
	{	this.stopTS = v;		}

	public Integer getExitStatus()
	{	return this.exitStatus;	}

	public void setExitStatus(Integer v)
	{	this.exitStatus = v;	}

	public String getStdOut()
	{	return this.stdOut;		}

	public void setStdOut(String v)
	{	this.stdOut = v;		}

	public String getStdErr()
	{	return this.stdErr;		}

	public void setStdErr(String v)
	{	this.stdErr = v;		}


	public ExecThreadEvent()
	{
		this.startTS = null;
		this.stopTS = null;
		this.exitStatus = null;
		this.stdOut = null;
		this.stdErr = null;
	}

	public void resetStds()
	{
		this.stdOut = null;
		this.stdErr = null;
	}

	public void set(final ExecThreadEvent toCopy)
	{
		this.startTS		= toCopy.startTS;
		this.stopTS			= toCopy.stopTS;
		this.exitStatus		= toCopy.exitStatus;
		this.stdOut			= toCopy.stdOut;
		this.stdErr			= toCopy.stdErr;
	}

	public ExecThreadEvent getCopy()
	{
		ExecThreadEvent copy = new ExecThreadEvent();
		copy.set(this);

		return copy;
	}

}