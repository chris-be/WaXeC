/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.exec;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import fr.bcm.lib.sil.file.FileWatchConf;
import fr.bcm.lib.sil.file.Watcher;
import fr.bcm.lib.sil.patterns.IObservable;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.lib.sil.patterns.TypedObserverMap;
import fr.bcm.lib.sil.thread.PeriodicThreadBase;
import fr.bcm.waxec.Defines;

/**
 * Thread watching files
 */
public class WatcherThread extends PeriodicThreadBase implements IObservable<WatcherThreadEvent> {

	/** Directory to watch */
	protected FileWatchConf rootConf;

	/** Observers */
	protected TypedObserverMap	observers = new TypedObserverMap();

	/**
	 * Root directory to watch
	 * @param dir
	 * @param files File name / [*?]
	 */
	public WatcherThread(String dir, Iterable<String> files)
	{
		super(Defines.WATCH_WAIT_DELAY_MS);

		Path root = Paths.get(dir);
		if(Files.notExists(root))
		{
			throw new IllegalArgumentException();
		}

		this.rootConf = new FileWatchConf(root, true, files);
	}

	@Override
	public void run()
	{
		try
		{
			this.watch();
		}
		catch(InterruptedException ie)
		{	// Thread stopped
		}
		catch(IOException e)
		{
			this.unhandledException(e);
		}
	}

	protected void watch() throws IOException, InterruptedException
	{
		Watcher watcher = new Watcher();
		watcher.addWatch(this.rootConf);

		boolean goOn = true;
		TreeSet<String> changedFiles = new TreeSet<>();
		while(goOn)
		{
			int sleepDelayMs = this.timer.getSleepDelay();
			if(watcher.poll(sleepDelayMs, TimeUnit.MILLISECONDS, changedFiles))
			{
				if(!watcher.isEmpty() && !changedFiles.isEmpty())
				{
					WatcherThreadEvent event = new WatcherThreadEvent(changedFiles);
					this.fireEvent(event, this);

					changedFiles.clear();
				}
			}
		}
	}

	@Override
	public void addObserver(IObserver<WatcherThreadEvent> observer)
	{
		this.observers.add(WatcherThreadEvent.class, observer);
	}

	@Override
	public void removeObserver(IObserver<WatcherThreadEvent> observer)
	{
		this.observers.remove(WatcherThreadEvent.class, observer);
	}

	protected void fireEvent(final WatcherThreadEvent event, final Object source)
	{
		this.observers.fireEvent(event, source);
	}

}