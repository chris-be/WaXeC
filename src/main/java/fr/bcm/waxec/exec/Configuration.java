/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.DebugBox;

/**
 * Configuration of program
 */
public class Configuration {

	public static String PATTERN_WORK_DIRECTORY		= "$WOD";
	public static String PATTERN_WATCH_DIRECTORY	= "$WAD";

	/**  Description */
	protected String description;

	/** Command */
	protected String exec;

	/** Parameters */
	protected ArrayList<String> params;

	/** Working directory */
	protected String workDir;

	/** Watch "on" */
	protected boolean watch;

	/** Watched directory */
	protected String watchDir;

	/** Watched files */
	protected ArrayList<String> watchFiles;

	public String getDescription()
	{	return this.description;	}

	public void setDescription(String v)
	{	this.description = v;		}

	public String getExec()
	{	return this.exec;			}

	public void setExec(String v)
	{	this.exec = v;				}

	public ArrayList<String> getParams()
	{	return this.params;			}

	public String getWorkDir()
	{	return this.workDir;		}

	public void setWorkDir(String v)
	{	this.workDir = v;			}

	public boolean isWatch()
	{	return this.watch;			}

	public void setWatch(boolean v)
	{	this.watch = v;				}

	public String getWatchDir()
	{	return this.watchDir;		}

	public void setWatchDir(String v)
	{	this.watchDir = v;			}

	public ArrayList<String> getWatchFiles()
	{	return this.watchFiles;		}


	public Configuration()
	{
		this.params = new ArrayList<>();
		this.watchFiles = new ArrayList<>();
	}

	protected void ensureComplient()
	{
		if(this.params == null)
		{
			this.params = new ArrayList<>();
		}

		if(this.watchFiles == null)
		{
			this.watchFiles = new ArrayList<>();
		}
	}

	public void set(Configuration toCopy)
	{
		DebugBox.predicateNotNull(toCopy);
		
		this.description	= toCopy.description;

		this.exec			= toCopy.exec;
		CVS_Collection.replaceAll(this.params, toCopy.params);
		this.workDir		= toCopy.workDir;

		this.watch			= toCopy.watch;
		this.watchDir		= toCopy.watchDir;
		CVS_Collection.replaceAll(this.watchFiles, toCopy.watchFiles);
	}

	/**
	 * Returns params with patterns replaced
	 */
	public List<String> createFinalParams()
	{
		ArrayList<String> pms = new ArrayList<>();

		String pWod= "(?i)" + Matcher.quoteReplacement(PATTERN_WORK_DIRECTORY);
		String pWad= "(?i)" + Matcher.quoteReplacement(PATTERN_WATCH_DIRECTORY);

		String wod = (this.workDir != null) ? this.workDir : "";
		String wad = (this.watch && this.watchDir != null) ? this.watchDir : "";

		for(String org : this.params)
		{
			String pm = org;
			pm = pm.replaceAll(pWod, wod);
			pm = pm.replaceAll(pWad, wad);

			pms.add(pm);
		}

		return pms;
	}

	public static Configuration loadYAML(final File file)
	{
		DebugBox.predicateNotNull(file);
		if(!file.exists())
		{
			throw new IllegalArgumentException("Invalid file");
		}

		Configuration conf;
		try
		{
			ObjectMapper oMapper = new ObjectMapper(new YAMLFactory());
			conf = oMapper.readValue(file, Configuration.class);
			conf.ensureComplient();
		}
		catch(Exception e)
		{
			String msg = String.format("File [%s] could not be read/parsed: [%s]", file, e.getMessage());
			throw new IllegalArgumentException(msg);
		}

		return conf;
	}

	public static void saveYAML(final Configuration conf, final File file)
	{
		DebugBox.predicateNotNull(conf);
		DebugBox.predicateNotNull(file);

		try
		{
			ObjectMapper oMapper = new ObjectMapper(new YAMLFactory());
			oMapper.writeValue(file, conf);
		}
		catch(Exception e)
		{
			String msg = String.format("File [%s] could not be saved: [%s]", file, e.getMessage());
			throw new IllegalArgumentException(msg);
		}
	}

}