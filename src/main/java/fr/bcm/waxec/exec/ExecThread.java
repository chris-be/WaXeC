/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.exec;

import java.io.IOException;
import java.time.LocalDateTime;

import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.InputStream2String;
import fr.bcm.lib.sil.patterns.IObservable;
import fr.bcm.lib.sil.patterns.IObserver;
import fr.bcm.lib.sil.patterns.TypedObserverMap;
import fr.bcm.lib.sil.thread.PeriodicThreadBase;
import fr.bcm.waxec.Defines;

/**
 * Thread executing command
 */
public class ExecThread extends PeriodicThreadBase implements IObservable<ExecThreadEvent> {

	/** ProcessBuilder to use */
	protected ProcessBuilder processBuilder;

	/** Process */
	protected Process process;

	/** Observers */
	protected TypedObserverMap	observers = new TypedObserverMap();

	public ExecThread(ProcessBuilder processBuilder)
	{
		super(Defines.EXEC_WAIT_DELAY_MS);
		DebugBox.predicateNotNull(processBuilder);

		this.processBuilder = processBuilder;
		this.process = null;
	}

	@Override
	public void run()
	{
		if(this.process != null)
		{
			assert false : "Already running";
			return;
		}

		try
		{
			this.execute();
		}
		catch(InterruptedException iex)
		{	// Ignore
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	protected void execute() throws IOException, InterruptedException
	{
		boolean goOn = true;

		LocalDateTime start = LocalDateTime.now();
		this.process = this.processBuilder.start();

		ExecThreadEvent event = new ExecThreadEvent();
		event.setStartTS(start);

		try(
			  InputStream2String stdOut = new InputStream2String(process.getInputStream())
			; InputStream2String stdErr = new InputStream2String(process.getErrorStream())
		 )
		{
			this.fireEvent(event, this);
			this.timer.start();

			while(goOn)
			{
				try
				{	// Wait
					this.timer.sleepIfNeeded();
				}
				catch(InterruptedException ie)
				{	// Thread was asked to stop
					goOn = false;
				}

				if(!this.process.isAlive())
				{	// Process ended while sleeping
					LocalDateTime stop = LocalDateTime.now();
					event.setStopTS(stop);
					event.setExitStatus(this.getExitStatus());
					goOn = false;
				}

				// Get process status
				this.updateEvent(event, stdOut, stdErr);
				if((event.getStdOut() != null) || (event.getStdErr() != null))
				{
					this.fireEvent(event, this);
					event.resetStds();
				}
			}

			// End (process finished or user asked to stop)
			stdOut.close();
			stdErr.close();

			if(this.process.isAlive())
			{
				// Stopped by user: kill process
				this.process.destroy();
			}
			this.process = null;

			this.fireEvent(event, this);
		}
	}

	protected Integer getExitStatus()
	{
		try
		{
			return this.process.exitValue();
		}
		catch(IllegalThreadStateException ite)
		{

		}

		return null;
	}

	protected void updateEvent(ExecThreadEvent event, InputStream2String stdOut, InputStream2String stdErr) throws IOException
	{
		if(stdOut.readAvailable())
		{
			event.setStdOut(stdOut.getText());
			stdOut.clear();
		}

		if(stdErr.readAvailable())
		{
			event.setStdErr(stdErr.getText());
			stdErr.clear();
		}
	}

	@Override
	public void addObserver(IObserver<ExecThreadEvent> observer)
	{
		this.observers.add(ExecThreadEvent.class, observer);
	}

	@Override
	public void removeObserver(IObserver<ExecThreadEvent> observer)
	{
		this.observers.remove(ExecThreadEvent.class, observer);
	}

	protected void fireEvent(final ExecThreadEvent event, final Object source)
	{
		this.observers.fireEvent(event.getCopy(), source);
	}

}