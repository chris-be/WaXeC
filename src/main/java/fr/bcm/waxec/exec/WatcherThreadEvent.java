/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec.exec;

import java.util.ArrayList;

import fr.bcm.lib.sil.CVS_Collection;
import fr.bcm.lib.sil.patterns.IEvent;

/**
 * @see WatcherThread
 * 
 */
public class WatcherThreadEvent implements IEvent {

	/** List of changed files */
	protected ArrayList<String> fileNames;

	public ArrayList<String> getFileNames()
	{
		return this.fileNames;
	}

	public WatcherThreadEvent()
	{
		this.fileNames = new ArrayList<>();
	}

	public WatcherThreadEvent(Iterable<String> fileNames)
	{
		this();
		this.addFileNames(fileNames);
	}

	public void addFileNames(Iterable<String> fileNames)
	{
		CVS_Collection.addAll(this.fileNames, fileNames);
	}

	public void set(final WatcherThreadEvent toCopy)
	{
		CVS_Collection.replaceAll(this.fileNames, toCopy.fileNames);
	}

	public WatcherThreadEvent getCopy()
	{
		WatcherThreadEvent copy = new WatcherThreadEvent();
		copy.set(this);

		return copy;
	}

}