/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec;

import javax.swing.SwingUtilities;

import fr.bcm.waxec.gui.MainWindow;

/**
 * Program
 */
public class Main {

	public static void main(String[] args) {

		int exitCode = 0;

		try
		{
			MainWindow wnd = new MainWindow();

			SwingUtilities.invokeLater(new Runnable() {
	
				@Override
				public void run()
				{
					wnd.setVisible(true);
				}
			});

			// Test
			// System.runFinalizersOnExit(true);

			while(!wnd.isVisible())
			{
				Thread.sleep(500L);
			}

			while(wnd.isVisible())
			{
				Thread.sleep(1000L);
			}

			// Test
			System.gc();
			System.runFinalization();
		}
		catch(InterruptedException ie)
		{
			System.err.print("Exception caught: ");
			System.err.println(ie.getMessage());
			exitCode = 1;
		}
		catch(Exception ex)
		{
			System.err.print("Exception caught: ");
			System.err.println(ex.getMessage());
			ex.printStackTrace();
			exitCode = 1;
		}

		System.exit(exitCode);
	}

}