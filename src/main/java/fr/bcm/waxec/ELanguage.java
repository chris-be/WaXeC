/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec;

import java.util.Locale;

import fr.bcm.lib.sil.DebugBox;

/**
 * Language supported by app 
 *
 */
public enum ELanguage {

	/**
	 * English
	 */
	  ENGLISH		("en", "GB")

	/**
	 * French
	 */
	, FRANCAIS		("fr", "FR")

	/**
	 * German
	 */
	//, DEUTSCH		("de", "DE")

	;

	private Locale locale;

	/**
	 * Get locale
	 */
	public Locale getLocale()
	{
		return this.locale;
	}

	/**
	 * Get operator representation
	 */
	public String getISO3Code()
	{
		return this.locale.getISO3Language();
	}

	private ELanguage(String language, String country)
	{
		DebugBox.predicateNotNull(language);
		DebugBox.predicateNotNull(country);

		this.locale = new Locale(language, country);
		DebugBox.predicateNotNull(this.locale);
	}

	/**
	 * Get default language to use depending on system
	 */
	public static ELanguage getDefault()
	{
		Locale locale = Locale.getDefault();
		String code = locale.getISO3Language();

		ELanguage lg = ELanguage.ENGLISH;
		if(code != null)
		{	// Find out enum if possible
			for(ELanguage test : ELanguage.values())
			{
				if(code.compareToIgnoreCase(test.getISO3Code()) == 0)
				{
					lg = test;
					break;
				}
			}
		}

		return lg;
	}

	public ELanguage getNext()
	{
		ELanguage[] list = ELanguage.values();
		int selIdx = 0;
		for(int i = 0 ; i < list.length-1 ; ++i)
		{
			if(list[i] == this)
			{
				selIdx = i+1;
				break;
			}
		}

		return list[selIdx];
	}

}