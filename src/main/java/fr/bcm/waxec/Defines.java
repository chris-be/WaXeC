/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec;

import java.awt.Image;
import java.io.File;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import fr.bcm.lib.sil.CVS_Jar;
import fr.bcm.lib.sil.DebugBox;
import fr.bcm.lib.sil.file.FileExtensions;
import fr.bcm.lib.sil.file.InputStream2String;
import fr.bcm.lib.sil.rsrc.IResourceLoader;

/**
 * Some defines of program
 */
public class Defines {

	public static final String TITLE		= "WaXeC - Watch & Exec";
	public static final String SHORT_TITLE	= "WaXeC";

	public static final String VERSION		= "0.1.0a - Remain some todo and cleaning";
	public static final String LICENSE_TYPE = "MIT";

	private static final File LICENSE_FILE	= new File("LICENSE");

	public static final String AUTHOR		= "Christophe Marc BERTONCINI";


	public static final String APP_TITLE	= Defines.TITLE;

	public static final String CONFIG_EXTENSION			= "wxc";

	public static final String CONFIG_EXTENSION_YAML	= CONFIG_EXTENSION + "-yaml";

	// Delay between each check of process
	public static final int EXEC_WAIT_DELAY_MS	= 500;

	// Delay between each poll
	public static final int WATCH_WAIT_DELAY_MS	= 500;

	private static TreeMap<ELanguage, String>	language2Flagname;

	static
	{
		TreeMap<ELanguage, String> map = new TreeMap<>();
		map.put(ELanguage.ENGLISH, "United_Kingdom");
		map.put(ELanguage.FRANCAIS, "France");

		Defines.language2Flagname = map;
	}

	public static String getLicense()
	{
		String lic;

		try(IResourceLoader loader = CVS_Jar.getResourceLoader())
		{
			try(InputStream is = loader.openResource(Defines.LICENSE_FILE))
			{
				try(InputStream2String r = new InputStream2String(is))
				{
					r.readAll();
					lic = r.getText();
				}
			}
		}
		catch(Exception e)
		{
			lic = "Error reading license file [" + e.getMessage() + "]";
		}

		return lic;
	}

	public static File getFlagFile(ELanguage language)
	{
		String filename = Defines.language2Flagname.get(language);
		if(filename != null)
		{
			filename = filename + "_24x16.png";
		}
		else
		{
			DebugBox.unhandledEnum(language); 
		}

		return new File(filename);
	}

	public static ImageIcon getFlagIcon(ELanguage language)
	{
		ImageIcon icon;

		File file = Defines.getFlagFile(language);
		try(IResourceLoader loader = CVS_Jar.getResourceLoader())
		{
			try(InputStream is = loader.openResource(file))
			{
				Image img = ImageIO.read(is);
				icon = new ImageIcon(img);
			}
		}
		catch(Exception e)
		{
			icon = new ImageIcon();
		}

		return icon;
	}

	public static FileExtensions getConfigExtensions()
	{
		FileExtensions fe = new FileExtensions();
		fe.addExtension(Defines.CONFIG_EXTENSION);
		fe.addExtension(Defines.CONFIG_EXTENSION_YAML);
		return fe;
	}

	public static String formatTimeStamp(final LocalDateTime time)
	{
		DebugBox.predicateNotNull(time);
		return time.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

}