/*
  Copyright 2017 Christophe Marc BERTONCINI

  This file is part of WaXeC - Watch & Execute.
   For the full copyright and license information, please refer to
   the file named LICENSE that was distributed with this source code.
*/

package fr.bcm.waxec;

import java.awt.Color;

/**
 * Some color definitions
 */
public class Colors {

	public static final Color LIST_PANEL_BK_COLOR	= new Color(224, 232, 240, 255);

	public static final Color CONF_PANEL_BK_COLOR	= new Color(180, 255, 200, 255);

	//static final Color EXEC_PANEL_BK_COLOR = new Color(116, 116, 218, 255);
	public static final Color EXEC_PANEL_BK_COLOR	= new Color( 10, 255, 128, 255); // a:128

	public static final Color ERROR_BK_COLOR		= new Color(255,  90,  90, 128);
	public static final Color OK_BK_COLOR			= new Color(148, 255,  90, 128);

}