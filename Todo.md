# GUI
- create ChooseLanguageDialog with buttons
- report "start/stop": enable vertical scrolling
- list of executions: selecting one execution shows its report
- action "help":
	- execute command with help argument (depending on OS : -h, --help, /?)
	- show output in a popup

# MISC
- configuration of waxec
- option to exec without gui
- more options for watched files:
	- force process if no files are modified (were already modified)
	- option: call process with each "added/modified" file ($WFILE)
- functionality "save logs"

# Bug
- Investigate why there are problems with "ls", "find" command arguments (when newline is used it works).

# Another way ??
- exec config
- "multi exec" config : call exec config (&&, ||)
- watcher config calling "[multi] exec config"
