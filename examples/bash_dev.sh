#!/bin/bash
export HISTFILE=~/.bash_history_dev
setterm --foreground blue

alias ssh_dev='ssh -XC -c blowfish-cbc,arcfour'

echo "Dev Console"
echo " - specific history file"
echo " - specific ssh encryption"

bash
